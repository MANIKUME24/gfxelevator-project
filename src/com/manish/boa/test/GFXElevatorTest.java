package com.manish.boa.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.manish.boa.GFXElevator;

public class GFXElevatorTest {
	
	List<String> lines = new ArrayList<String>();
	
	@Before
	public void setUp() throws Exception {
		lines.add("10:8-1");
		lines.add("9:1-5,1-6,1-5");
		lines.add("2:4-1,4-2,6-8");
		lines.add("3:7-9,3-7,5-8,7-11,11-1");
		lines.add("7:11-6,10-5,6-8,7-4,12-7,8-9");
		lines.add("6:1-8,6-8");
		lines.add("1:12-1,1-12");
		lines.add("4:4-6,6-8");
	}

	@Test
	public void testOperateElevatorInModeA() {
		
		Map<Integer,List<String>> modeAOutputMap = GFXElevator.operateElevatorInModeA(lines);
	    
	    List<String> outputList = Arrays.asList("10","8","1","(9)");
        assertEquals(outputList, modeAOutputMap.get(1));
         
        outputList = Arrays.asList("9","1","5","1","6","1","5","(30)");
        assertEquals(outputList, modeAOutputMap.get(2));
        
        outputList = Arrays.asList("2","4","1","4","2","6","8","(16)");
        assertEquals(outputList, modeAOutputMap.get(3));
        
        outputList = Arrays.asList("3","7","9","3","7","5","8","7","11","11","1","(36)");
        assertEquals(outputList, modeAOutputMap.get(4));
        
        outputList = Arrays.asList("7","11","6","10","5","6","8","7","4","12","7","8","9","(40)");
        assertEquals(outputList, modeAOutputMap.get(5));
        
        outputList = Arrays.asList("6","1","8","6","8","(16)");
        assertEquals(outputList, modeAOutputMap.get(6));
        
        outputList = Arrays.asList("1","12","1","1","12","(33)");
        assertEquals(outputList, modeAOutputMap.get(7));
        
        outputList = Arrays.asList("4","4","6","6","8","(4)");
        assertEquals(outputList, modeAOutputMap.get(8));
        
        
        
	}

	@Test
	public void testOperateElevatorInModeB() {
		
		Map<Integer,List<String>> modeBOutputMap = GFXElevator.operateElevatorInModeB(lines);
		
        List<String> outputList = Arrays.asList("10","8","1","(9)");
        assertEquals(outputList, modeBOutputMap.get(1));
        
        outputList = Arrays.asList("9","1","5","6","(13)");
        assertEquals(outputList, modeBOutputMap.get(2));
        
        outputList = Arrays.asList("2","4","2","1","6","8","(12)");
        assertEquals(outputList, modeBOutputMap.get(3));
        
        outputList = Arrays.asList("3","3","5","7","8","9","11","1","(18)");
        assertEquals(outputList, modeBOutputMap.get(4));
        
        outputList = Arrays.asList("7","12","11","10","7","6","5","4","8","9","(18)");
        assertEquals(outputList, modeBOutputMap.get(5));
        
        outputList = Arrays.asList("6","1","6","8","(12)");
        assertEquals(outputList, modeBOutputMap.get(6));
        
        outputList = Arrays.asList("1","12","1","(22)");
        assertEquals(outputList, modeBOutputMap.get(7));
        
        outputList = Arrays.asList("4","4","6","8","(4)");
        assertEquals(outputList, modeBOutputMap.get(8));
	}

}
