package com.manish.boa;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Manish Agrawal
 *
 */
public class GFXElevator {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//Check that at least 2 arguments are passed in input
		if(args.length > 0 && args.length == 2){
			
			//Read the 1st input into a file
			FileReader fileReader = new FileReader(args[0]);
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        List<String> lines = new ArrayList<String>();
	        String line = null;
	        //Write all input from file into a String list which will be be processed at once
	        while ((line = bufferedReader.readLine()) != null) {
	            lines.add(line);
	        }
	        bufferedReader.close();
	        String mode = args[1];
	    	Map<Integer,List<String>> OutputMap = new HashMap<Integer,List<String>>();
	    	//Check if 2nd argument is mode A & if yes process all inputs from file in mode A
	    	if(!mode.isEmpty() && mode.trim().equals("A")){
	    		OutputMap = operateElevatorInModeA(lines);
			}else if(!mode.isEmpty() && mode.trim().equals("B")){ //Check if 2nd argument is mode B & if yes process all inputs from file in mode B
				OutputMap = operateElevatorInModeB(lines);
			}else{
				System.out.println("Please specify the correct mode A or B");
			}
			
		}else{
			System.out.println("Please provide the file name & mode of operation for elevator");
		}

	}
	
	//mode A method
	public static Map<Integer, List<String>> operateElevatorInModeA(List<String> modeAInputs){
		
		Map<Integer,List<String>> modaAOutputMap = new HashMap<Integer,List<String>>();
		int mapKey = 1;
		
		for(String modeAInput : modeAInputs){
			List<String> elevatorRoute = new ArrayList<String>();
			Integer sumfloorTravelled = 0;
			//Split each input 1st based on : to get elevator's starting position
			String[] arr1 = modeAInput.split(":");
		
			//Add starting position of elevator to route of elevator
			elevatorRoute.add(arr1[0]);
			
			//Split the 2nd String of arr1 based on , to get floor intervals the elevator need to travel 
			String[] arr2 = arr1[1].split(",");
			for(int i=0;i<arr2.length;i++){
				String arr3[] = arr2[i].split("-");
				
				//Trace each route of elevator
				elevatorRoute.add(arr3[0]);
				elevatorRoute.add(arr3[1]);
				
				
			}
			//Calculate the total number of floors traveled by elevator
			for(int i =0;i<elevatorRoute.size()-1;i++){
				sumfloorTravelled = sumfloorTravelled + Math.abs(Integer.parseInt(elevatorRoute.get(i)) - Integer.parseInt(elevatorRoute.get(i+1)));
			}
			elevatorRoute.add("("+String.valueOf(sumfloorTravelled)+")");
			modaAOutputMap.put(mapKey,elevatorRoute);
			mapKey++;
		}
		return modaAOutputMap;
	}

	//mode B method
	public static Map<Integer, List<String>> operateElevatorInModeB(List<String> modeBInputs){
		
		Map<Integer,List<String>> modaBOutputMap = new HashMap<Integer,List<String>>();
		int mapKey = 1;
		
		for(String modeBInput : modeBInputs){
			List<String> elevatorroute = new ArrayList<String>();
			//Split each input 1st based on : to get elevator's starting position
			String[] arr1 = modeBInput.split(":");
			elevatorroute.add(arr1[0]);
			//Split the 2nd String of arr1 based on , to get floor intervals the elevator need to travel 
			String[] arr2 = arr1[1].split(",");
			List<Integer> upRoute = new ArrayList<Integer>();
			List<Integer> downRoute = new ArrayList<Integer>();
			
			/*
			 * For each floor interval check whether elevator need to go up or down.
			 * If elevator need to go up then add the starting & ending floor to upRoute list.
			 * If elevator need to go down then add the starting & ending floor to downRoute list.
			 * 
			 * */
			for(int i=0;i<arr2.length;i++){
				String[] arr3 = arr2[i].split("-");
				if(Integer.parseInt(arr3[0]) - Integer.parseInt(arr3[1]) < 0){
					if(!upRoute.contains(Integer.parseInt(arr3[0]))){
						upRoute.add(Integer.parseInt(arr3[0]));
					}
					if(!upRoute.contains(Integer.parseInt(arr3[1]))){
						upRoute.add(Integer.parseInt(arr3[1]));
					}
				}else{
					if(!downRoute.contains(Integer.parseInt(arr3[0]))){
						downRoute.add(Integer.parseInt(arr3[0]));
					}
					if(!downRoute.contains(Integer.parseInt(arr3[1]))){
						downRoute.add(Integer.parseInt(arr3[1]));
					}
				}
			}
			
			//Sort the upRoute on ascending order
			Collections.sort(upRoute);
			
			//Sort the downRoute on descending order
			Collections.sort(downRoute);
			Collections.reverse(downRoute);
			
			
			/*
			 * Now Check whether elevator need to go up more or down more.
			 * If elevator need to go up more then start adding the upRoute to elevator starting position first & then add downRoute
			 * Else if elevator need to go down more then start adding the downRoute to elevator starting position first & then add upRoute 
			 */
			if(upRoute.size() - downRoute.size() > 0){
				for(Integer up : upRoute){
					if(!elevatorroute.subList(1, elevatorroute.size()).contains(String.valueOf(up))){
						elevatorroute.add(String.valueOf(up));
					}
				}
				for(Integer down : downRoute){
					if(!elevatorroute.subList(1, elevatorroute.size()).contains(String.valueOf(down))){
						elevatorroute.add(String.valueOf(down));
					}
				}
			}else{
				
				for(Integer down : downRoute){
					if(!elevatorroute.subList(1, elevatorroute.size()).contains(String.valueOf(down))){
						elevatorroute.add(String.valueOf(down));
					}
				}
				for(Integer up : upRoute){
					if(!elevatorroute.subList(1, elevatorroute.size()).contains(String.valueOf(up))){
						elevatorroute.add(String.valueOf(up));
					}
				}
			}
			
			Integer sumfloorTravelled = 0;
			//Calculate total number of floors traveled by elevator
			for(int i =0;i<elevatorroute.size()-1;i++){
				sumfloorTravelled = sumfloorTravelled + Math.abs(Integer.parseInt(elevatorroute.get(i)) - Integer.parseInt(elevatorroute.get(i+1)));
			}
			
			elevatorroute.add("("+String.valueOf(sumfloorTravelled)+")");
			modaBOutputMap.put(mapKey,elevatorroute);
			mapKey++;
		}
		
		return modaBOutputMap;
		
	}
}
